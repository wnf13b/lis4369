#LIS 4369
##Nick Fennell

###Program Requirements

1. Create a room class
2. Create a create a constructor with and without parameters
3. Create getter/setter methods
4. Display correct data in two decimal places

![Running](img/run.png)
