﻿using System;

namespace ConsoleApplication
{
    public class Program
    {
        public static void Main(string[] args)
        {
            
        	DateTime date = DateTime.Now;

        	Console.WriteLine(date);
        	Console.WriteLine("Author: Nick Fennell");
        	Console.WriteLine();

        	Room r1 = new Room();

        	Console.WriteLine();
        	r1.displayRoom(r1.getType(), r1.getHeight(), r1.getLength(), r1.getWidth());
        	Console.WriteLine("Room Area: " + r1.getArea() + " sq ft");
        	Console.WriteLine("Room Volume: "+r1.getVolume() + " cu ft");
        	Console.WriteLine("Room Volume: "+r1.getVolumeyd() + " cu yd");

        	Console.WriteLine();
        	Console.WriteLine("Modify Default Values");

        	Console.WriteLine();        	
        	Console.Write("Room Type: ");
        	r1.setType(Console.ReadLine());
        	
        	Console.Write("Room Height: ");
        	r1.setHeight(Convert.ToDouble(Console.ReadLine()));
        	
        	Console.Write("Room Length: ");
        	r1.setLength(Convert.ToDouble(Console.ReadLine()));
        	
        	Console.Write("Room Width: ");
        	r1.setWidth(Convert.ToDouble(Console.ReadLine()));

        	Console.WriteLine();
        	Console.WriteLine("New values for modified room");

        	Console.WriteLine();
        	r1.displayRoom(r1.getType(), r1.getHeight(), r1.getLength(), r1.getWidth());
        	Console.WriteLine("Room Area: " + r1.getArea() + " sq ft");
        	Console.WriteLine("Room Volume: "+r1.getVolume() + " cu ft");
        	Console.WriteLine("Room Volume: "+r1.getVolumeyd() + " cu yd");

        	Console.WriteLine();

       		Console.WriteLine();
       		string newT;
       		double newL, newW, newH;


			Console.Write("Room Type: ");
        	newT = Console.ReadLine();
        	
        	Console.Write("Room Height: ");
        	newH = Convert.ToDouble(Console.ReadLine());
        	
        	Console.Write("Room Length: ");
        	newL = Convert.ToDouble(Console.ReadLine());
        	
        	Console.Write("Room Width: ");
        	newW = Convert.ToDouble(Console.ReadLine());

        	Room r2 = new Room(newT, newL, newW, newH);

        	Console.WriteLine();

        	r2.displayRoom(r2.getType(), r2.getHeight(), r2.getLength(), r2.getWidth());
        	Console.WriteLine("Room Area: " + r2.getArea() + " sq ft");
        	Console.WriteLine("Room Volume: "+r2.getVolume() + " cu ft");
        	Console.WriteLine("Room Volume: "+r2.getVolumeyd() + " cu yd");

        	Console.WriteLine();
        	Console.Write("Press any key to exit: ");
        	Console.ReadKey();


        }
    }
}
