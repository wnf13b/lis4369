using System;

public class Room{

	private string type;
	private double length, width, height;

	public Room(){
	
	  type = "Default";
	  length = 10.0;
	  width = 10.0;
	  height = 10.0;
	  Console.WriteLine("Creating "+ this.type +" room object with no parameters");

	}

	public Room(string nType, double nLen, double nWid, double nHeight){

	  type = nType;
	  length = nLen;
	  width = nWid;
	  height = nHeight;
	  Console.WriteLine("Creating "+ this.type +" room constructor with four parameters");

	}


	public void setType(string newT){
	  
	  type = newT;

	}
	

	public void setHeight(double newH){
	  
	  height = newH;

	}

	public void setWidth(double newW){
	  
	  width = newW;

	}

	public void setLength(double newL){
	  
	  length = newL;

	}

	public string getType(){

	  return type;

	}

	public double getHeight(){

	  return height;

	}

	public double getWidth(){

	  return width;

	}

	public double getLength(){

	  return length;

	}

	public string getArea(){

		return String.Format("{0:0.00}", (width*length));

	}

	public string getVolume(){

		return String.Format("{0:0.00}", (width*length*height));

	}

	public string getVolumeyd(){

		return String.Format("{0:0.00}", ((width*length*height)*0.037037));

	}

	public void displayRoom(string typeC, double heightC, double lengthC, double widthC){

		Console.WriteLine("Room Type: " + typeC);
		Console.WriteLine("Room Height: " + heightC);
		Console.WriteLine("Room Width: " + widthC);
		Console.WriteLine("Room Length: " +lengthC);

	}

}

