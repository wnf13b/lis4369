﻿using System;

namespace ConsoleApplication
{
    public class Program
    {
        public static void Main(string[] args)
        {
          
	  DateTime date = DateTime.Now;

	  Console.WriteLine("Program Requirements:\nA3 - Future Value Calculator"); 
	  Console.WriteLine("Author: Will Fennell");
	  Console.WriteLine("1) Display Date and Time\n2) Research Future Value Formula\n3) Use Data Validation\n4) Create a FutureValue method with specified parameters");
	  Console.WriteLine("-----------------------------------------\nNow: " + date);

	  int numYears;
	  decimal monthlyDep, yearlyInt, presentValue, futureValue;

	  Console.WriteLine("\n");

	  bool validBal = false;

	  do{	

		  Console.Write("Starting Balance: ");
		  string userInput = Console.ReadLine();

		  validBal = (decimal.TryParse(userInput, out presentValue));

		  if(!validBal){
		  	Console.WriteLine("Must be a decimal value");
		  }

		 }while(!validBal);

		 bool validYear = false;

	  do{	

		  Console.Write("Term (Years): ");
		  string userInput = Console.ReadLine();

		  validYear = (int.TryParse(userInput, out numYears));

		  if(!validYear){
		  	Console.WriteLine("Must be an integer value");
		  }

		 }while(!validYear);


	  bool validInt = false;


	  do{	

		  Console.Write("Interest Rate: ");
		  string userInput = Console.ReadLine();

		  validInt = (decimal.TryParse(userInput, out yearlyInt));

		  if(!validInt){
		  	Console.WriteLine("Must be a decimal value");
		  }

		 }while(!validInt);


	  bool validDeposit = false;

	  do{	

		  Console.Write("Monthly Deposit: ");
		  string userInput = Console.ReadLine();

		  validDeposit = (decimal.TryParse(userInput, out monthlyDep));

		  if(!validDeposit){
		  	Console.WriteLine("Must be an integer value");
		  }

		 }while(!validDeposit);


		Console.WriteLine("\n\n*** Future Value ***");

		futureValue = FutureValue(presentValue, numYears, yearlyInt, monthlyDep);

		string currency = futureValue.ToString("C");

		Console.WriteLine(currency);

		Console.WriteLine("\nEnter Any Key to Exit!");
		Console.ReadKey();

	}

	public static decimal FutureValue(decimal current, int years, decimal rate, decimal payment){

		decimal finalVal = 0;

		rate /= 100;

		double rOverk = ((double)rate/12.0);
		double nTimesk = (years*12);
		decimal afterPower = (decimal)(Math.Pow((1+rOverk),nTimesk));

		decimal firstVal = (current * afterPower);
		decimal secondVal = payment*((afterPower - 1)/(decimal)(rOverk));

		finalVal = firstVal + secondVal;

		return finalVal;

	}

    }
}
