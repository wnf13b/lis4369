# LIS4369

## Nick Fennell

### Assignment 2 Requirements:

*Three Parts*

1. Create Final Value Calculator Application
2. Display Date and Time
3. Use Data Validation
4. Research Final Value Formula

#### README.md file should include the following items:

* Screenshot of running application
* Screenshot of incorrect input

![Correct Input](img/right.png)
![Wrong Input](img/wrong.png)
