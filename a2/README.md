# LIS4369

## Nick Fennell

### Assignment 2 Requirements:

*Three Parts*

1. Create Calculator Application
2. Display Date and Time
3. Make sure you cannot divide by zero (EC)

#### README.md file should include the following items:

* Screenshot of valid calculation
* Screenshot of invalid selection
* Screenshot of not being able to divide by zero



![Correct Input](img/right.png)
![Wrong Input](img/wrong.png)
![Divide By Zero](img/zero.png)
