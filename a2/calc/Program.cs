﻿using System;

namespace ConsoleApplication
{
    public class Program
    {
        public static void Main(string[] args)
        {
	  
	  Console.WriteLine("A2: Simple Calculator Application");
	  Console.WriteLine("Author: Nick Fennell");

	  DateTime date = DateTime.Now;

	  Console.WriteLine("Date: "+date); 

	  
	  Console.WriteLine("----------------------------------");
	  Console.WriteLine();
	
	  Console.Write("Number 1: ");
	  
	  double num1 = Convert.ToDouble(Console.ReadLine());


	  Console.WriteLine();

	  Console.Write("Number 2: ");
 
	  double num2 = Convert.ToDouble(Console.ReadLine());

	  Console.WriteLine('\n');

	  Console.WriteLine("Which calculation would you like to perform?");
	  Console.WriteLine('\n');
	  
	  Console.WriteLine(" 1 - Addition \n\n 2 - Subtraction \n\n 3 - Divide \n\n 4 - Multiply \n\n");
	  
	  Console.Write("Your Choice: ");
	  int userInput = Convert.ToInt32(Console.ReadLine());
	  Console.WriteLine('\n');
	  double result = 0;

	  if(userInput > 4 || userInput < 1){	
		Console.WriteLine("Invalid Selection");
	  }else{

		switch(userInput){

		  case 1:
		    result = num1+num2;
		    Console.WriteLine(num1 + " + " + num2 + " = " + result);
		    break;
		
		  case 2:
		    result = num1-num2; 
		    Console.WriteLine(num1 + " - " + num2 + " = " + result);
		    break;

	          case 3:
		    if(num2 == 0){
			Console.WriteLine("Cannot Divide by Zero");	
		    }
		    else{
		    	result = num1/num2; 
		    	Console.WriteLine(num1 + " / " + num2 + " = " + result);
		    }
		    break;

		  case 4: 
		    result = num1*num2;
		    Console.WriteLine(num1 + " * " + num2 + " = " + result);
		    break;

		}


	  }


	  Console.Write("Enter Any Key To Exit!");
	  Console.ReadKey();

        }
    }
}
