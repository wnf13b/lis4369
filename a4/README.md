#LIS4369
##Nick Fennell

###Program Requirements

1. Create a Person class and a derived Student class
2. Allow access and modifying data through getter/setter methods
3. Properly display information to the user
4. Data validation

###Screenshots

![img/1.png](img/1.png)
