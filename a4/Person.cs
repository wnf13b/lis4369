using System;

public class Person{

	protected string fName;
	protected string lName;
	protected int age;

	public Person(){
	  
	  fName = "First name";
	  lName = "Last name";
	  age = 0;

	  Console.WriteLine("\nCreating base person object from default constructor (accepts no arguments):");

	  Console.WriteLine("Creating "+fName+" "+lName+" person object from default constructor (accepts no arguments): ");

	}

	public Person(string first="", string last="", int newage=0){

	  fName = first;
	  lName = last;
	  age = newage;

	}

	public void setFirst(string newF){

	  fName = newF;

	}

	public void setLast(string newL){

	  lName = newL;

	}

	public void setAge(int newA){

	  age = newA;

	}

	public string getFirst(){
	  
	  return fName;

	}

	public string getLast(){

	  return lName;

	}

	public int getAge(){

	  return age;

	}

	public virtual string GetObjectInfo()
		{
			return fName + " " + lName + " is " + age.ToString();
		}	

}
