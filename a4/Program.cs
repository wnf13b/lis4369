﻿using System;

namespace ConsoleApplication
{
    public class Program
    {
        public static void Main(string[] args)
        {

        	DateTime date = DateTime.Now;

        	Console.WriteLine("Now: "+date+"\n\n");

        	Person p1 = new Person();
        	Console.WriteLine("First: "+p1.getFirst());
        	Console.WriteLine("Last: "+p1.getLast());
        	Console.WriteLine("Age: "+p1.getAge()+"\n");

        	Console.WriteLine("Modify default constructor object data members \nUsing Getters/Setters");

        	int p_age;
	  
			Console.Write("First Name: ");
			p1.setFirst(Console.ReadLine());
			Console.Write("Last Name: ");
			p1.setLast(Console.ReadLine());
			Console.Write("Age: ");
            while (!int.TryParse(Console.ReadLine(), out p_age))
            {
            	Console.WriteLine("Age must be an integer.");
            }
            p1.setAge(p_age);
			Console.WriteLine();

			Console.WriteLine("Display object's new member data");
			Console.WriteLine("First Name: "+p1.getFirst());
			Console.WriteLine("Last Name: "+p1.getLast());
			Console.WriteLine("Age: "+p1.getAge());
	  		Console.WriteLine();

	  		Console.WriteLine("Call parameterized base constructor (accepts arguments):");

            Console.Write("First Name: ");
            string p_fname = Console.ReadLine();

            Console.Write("Last Name: ");
            string p_lname = Console.ReadLine();


            Console.Write("Age: ");
            while (!int.TryParse(Console.ReadLine(), out p_age))
            {
            	Console.WriteLine("Age must be an integer.");
            }

            Console.WriteLine();

            Person p2 = new Person(p_fname, p_lname, p_age);

            Console.Write("First Name: ");
            Console.WriteLine(p2.getFirst());

            Console.Write("Last Name: ");
            Console.WriteLine(p2.getLast());

            Console.Write("Age: ");
            Console.WriteLine(p2.getAge());

            Console.WriteLine("\nCall derived default constructor (inherits from base class):");

            Student s1 = new Student();

            Console.Write("First Name: ");
            Console.WriteLine(s1.getFirst());

            Console.Write("Last Name: ");
            Console.WriteLine(s1.getLast());

            Console.Write("Age: ");
            Console.WriteLine(s1.getAge());

            Console.WriteLine("\nDemonstrating Polymorphism");
            Console.WriteLine("(Calling parameterized base class constructor explicitly.)\n");

            Console.Write("First Name: ");
            string s_fname = Console.ReadLine();

            Console.Write("Last Name: ");
            string s_lname = Console.ReadLine();

            int s_age = 0;

            Console.Write("Age: ");
            while (!int.TryParse(Console.ReadLine(), out s_age))
            {
            	Console.WriteLine("Age must be an integer.");
            }

            Console.Write("College: ");
            string s_college = Console.ReadLine();

            Console.Write("Major: ");
            string s_major = Console.ReadLine();

            double s_gpa = 0.0;

            Console.Write("GPA: ");
            while (!double.TryParse(Console.ReadLine(), out s_gpa))
            {
            	Console.WriteLine("Age must be numeric.");
            }
            Console.WriteLine();

            Student s2 = new Student(s_fname, s_lname, s_age, s_college, s_major, s_gpa);

            Console.WriteLine();

            Console.Write("p2 - GetObjectInfo (virtual): \n");
            Console.WriteLine(p2.GetObjectInfo());

            Console.Write("\ns2 - GetObjectInfo (overridden): \n");
            Console.WriteLine(s2.GetObjectInfo());

            Console.WriteLine();
            Console.WriteLine("\nPress any key to exit!");
            Console.ReadKey();

        }
    }
}
