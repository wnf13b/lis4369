# 

## Nick Fennell

### Assignment 1 Requirements:

*Three Parts*

1. Installing and using Git and BitBucket
2. Installing and .NET
3. Create .NET Application and an ASP.NET application

#### README.md file should include the following items:

* Screenshot of hwapp
* Screenshot of aspnetcoreapp
* Git commands and descriptions

> #### Git commands w/short descriptions:

1. git init - create an empty git repository or reinitialize an existing one 
2. git status - show the working tree status
3. git add - add file contents to the index
4. git commit - record changes to the repository
5. git push - update remote references along with associated objects
6. git pull - fetch from and integrate with another repository or a local branch
7. git rm - remove files from the working tree and from the index

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![ASP.NET](img/asp.png)

*Screenshot of running aspnetcoreapp*:

![hwapp](img/hwapp.png)

*Screenshot of hwapp*:

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/wnf13b/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/wnf13b/myteamquotes/ "My Team Quotes Tutorial")
