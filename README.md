# LIS4369

## Nick Fennell

### Completed Assignments:

1. [Assignment 1](/a1/README.md "README.md")

    > This assignment had us install git and the .NET framework

2. [Assignment 2](/a2/README.md "README.md")

    > This assignment had us create a simple calculator in C#

3. [Assignment 3](/a3/README.md "README.md")

    > This assignment had us create a Future Value Calculator
    >
    > Takes in user input for
    >
    > > Rate
    >
    > > First investment
    >
    > > Money added each month
    >
    > > Total time in account

4. [Project 1](/p1/README.md "README.md")

    > This assignment had us create a Room Class
    >
    > Allowed us to modify the size of the room
    >
    > Printed out volume and area to user

5. [Assignment 4](/a4/README.md "README.md")

    > Created a person class and a derived student class
    >
    > Allowed users to modify that data
    >
    > Printed out information to user 

6. [Assignment 5](/a5/README.md "README.md")

    > Created a Vehicle class with a derived Car class
    >
    > Printed information to user
    >
    > Took in user input with data validation

7. [Project 2](/p2/README.md "README.md")

    > Completed the LINQ tutorial
    >
    > Learned how to select records from a LINQ set
    >
    > Used our knowledge to create a short program selecting specific people from the set
