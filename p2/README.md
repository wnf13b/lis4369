#LIS 4369
##Nick Fennell

###Program Requirements

1. Complete LINQ tutorial
2. Create a short program using LINQ statements
3. Display correct data to user

![Running](img/1.png)

![Running](img/2.png)
